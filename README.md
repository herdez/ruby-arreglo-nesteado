## Ejercicio - Formando arreglo nesteado

Define el método `nested_array` que recibe como argumento un `arreglo de strings` y un `factor`. Este factor determina el número de veces que se repetirá el proceso de juntar las palabras con el número de iteración en el que se encuentra este factor. 

El método deberá regresar un arreglo como el siguiente:

```ruby
#driver code

p nested_array(["uno", "dos", "tres", "cuatro"], 3) == [["uno", 1], ["uno", 2], ["uno", 3], ["dos", 1], ["dos", 2], ["dos", 3], ["tres", 1], ["tres", 2], ["tres", 3], ["cuatro", 1], ["cuatro", 2], ["cuatro", 3]]
p nested_array(["cinco"], 4) == [["cinco", 1], ["cinco", 2], ["cinco", 3], ["cinco", 4]]

```
